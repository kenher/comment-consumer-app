package com.bv.commentconsumerapp.dto;

import java.time.LocalDateTime;

public class Comment {
    public Long id;
    public LocalDateTime created;
    public String content;
}
