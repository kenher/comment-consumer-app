package com.bv.commentconsumerapp;

import com.fasterxml.jackson.core.JsonGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CommentConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommentConsumerApplication.class, args);
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer om() {
        return builder -> builder.featuresToDisable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
    }

}
