package com.bv.commentconsumerapp.service;

import com.bv.commentconsumerapp.configuration.SseConfiguration;
import com.bv.commentconsumerapp.dto.Comment;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.*;

@Slf4j
@Service
public class KafkaConsumerService implements ConsumerSeekAware {
    private final Collection<Comment> comments = Collections.synchronizedCollection(new ArrayList<>());

    private final SseConfiguration sseConfiguration;

    @Autowired
    public KafkaConsumerService(SseConfiguration sseConfiguration) {
        this.sseConfiguration = sseConfiguration;
    }

    public List<Comment> getExistingComments() {
        return new ArrayList<>(comments);
    }

    @KafkaListener(topics = "bv_comments", groupId = "bv_group")
    public void consume(Comment comment) {
        sseConfiguration.getEmitters().forEach(sseEmitter -> {
            try {
                sseEmitter.send(
                        SseEmitter.event()
                                .name("comment")
                                .id(String.valueOf(comment.id))
                                .data(comment));
            } catch (Exception e) {
                log.error("Emitter is already completed", e);
            }
        });

        comments.add(comment);
    }

    @Override
    public void onPartitionsAssigned(@NonNull Map<TopicPartition, Long> assignments, @NonNull ConsumerSeekCallback callback) {
        assignments.keySet().forEach(topicPartition -> callback.seekToBeginning(topicPartition.topic(), topicPartition.partition()));
    }
}
