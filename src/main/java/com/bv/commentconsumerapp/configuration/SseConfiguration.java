package com.bv.commentconsumerapp.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.*;

@Configuration
@EnableScheduling
public class SseConfiguration {
    private final Collection<SseEmitter> emitters = Collections.synchronizedCollection(new ArrayList<>());
    private final Collection<SseEmitter> markedToRemove = Collections.synchronizedCollection(new ArrayList<>());

    public void persistEmitter(SseEmitter emitter) {
        emitters.add(emitter);
    }

    public Collection<SseEmitter> getEmitters() {
        return emitters;
    }

    public void markToRemove(SseEmitter emitter) {
        markedToRemove.add(emitter);
    }

    // Cleanup data every 5 min
    @Scheduled(cron = "0 0/5 * * * *")
    public void cleanupEmitters() {
        if (markedToRemove.isEmpty()) return;

        emitters.removeAll(markedToRemove);
        markedToRemove.clear();
    }

}
