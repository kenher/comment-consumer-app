package com.bv.commentconsumerapp.controller;

import com.bv.commentconsumerapp.configuration.SseConfiguration;
import com.bv.commentconsumerapp.dto.Comment;
import com.bv.commentconsumerapp.service.KafkaConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("comments")
public class CommentController {
    private final SseConfiguration sseConfiguration;
    private final KafkaConsumerService kafkaConsumerService;

    @Autowired
    public CommentController(SseConfiguration sseConfiguration, KafkaConsumerService kafkaConsumerService) {
        this.sseConfiguration = sseConfiguration;
        this.kafkaConsumerService = kafkaConsumerService;
    }

    @GetMapping("exists")
    public List<Comment> getExistingComments() {
        return kafkaConsumerService.getExistingComments();
    }

    @GetMapping(value = "stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter getServerConnection() {
        var emitter = new SseEmitter(-1L);
        emitter.onCompletion(() -> sseConfiguration.markToRemove(emitter));
        emitter.onError(ex -> sseConfiguration.markToRemove(emitter));
        sseConfiguration.persistEmitter(emitter);

        return emitter;
    }
}
